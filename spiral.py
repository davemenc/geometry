from PIL import Image
from PIL import ImageDraw
import math

def draw_big_pix(draw,x,y,size,color):
    x = int(x)
    y = int(y)
    size = int(size)
    
    if size == 0:
        return
    elif size == 1:
        var_neg = 0
        var_pos = 0
    elif size == 2:
        var_neg=1
        var_pos=0
    elif size == 3:
        var_neg=1
        var_pos=1
    elif size == 4:
        var_neg=2
        var_pos=1
    elif size == 5:
        var_neg=2
        var_pos=2
    elif  size == 6:
        var_neg=3
        var_pos=2
    elif size == 7:
        var_neg=3
        var_pos=3
    else:
        var_neg = int(size/2)
        var_pos = int(size/2)
    #draw.point((x,y),fill=color)
    pix = []
    for h in range(x-var_neg,x+var_pos+1):
        for v in range (y-var_neg,y+var_pos+1):
            pix.append((h,v))
    #print x,y,size, var_neg,var_pos,pix
    draw.point(pix,fill=color)
        
def main():
    hpix = 300
    vpix = 300
    color = "#ff0000"
    outimage = "testimage.png"
     
    im = Image.new("RGB",(hpix,vpix))
    draw = ImageDraw.Draw(im)
    color = "#ff0000"
    TOTAL_RADIANS = math.pi*2
    PIX_SIZE = 1
    POINTS_PER_CIRCLES = 4000 # completely arbitrary
    SPIRAL_LEVELS = 3 #number of complete circles we're doing
    total_points = int(SPIRAL_LEVELS * POINTS_PER_CIRCLES)
    radian_inc = float(total_points)/TOTAL_RADIANS

    radius = hpix/2.1
    radius_decrease = .97#between .9 and .99
    center_x = hpix/2
    center_y = vpix/2

    for p in range(0,total_points):
        rad = radian_inc * p
        x = math.cos(rad) *radius + center_x
        y = math.sin(rad) * radius + center_y
        draw_big_pix(draw,x,y,PIX_SIZE,color)
        radius *= radius_decrease
    del draw

    im.save(outimage,"PNG")
    
if __name__ == "__main__":
    main()    

